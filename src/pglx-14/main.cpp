#include <cstring>
#include <iostream>

#include <sys/socket.h>
#include <netinet/ip.h>

#include <sys/unistd.h>

ssize_t write(int fd, const char* s)
{
    return ::write(fd, s, strlen(s));
    //return ::send(fd, s, strlen(s), 0);
}

int main()
{
    struct sockaddr_in server_addr;
    int server_fd;

    if((server_fd = socket(AF_INET, SOCK_STREAM, 0)) < 0)
    {
        // fails on windows, presumably a permission thing... except we aren't even listening yet
        perror("socket failed");
        return EXIT_FAILURE;
    }

    // Guidance from:
    // https://stackoverflow.com/questions/10619952/how-to-completely-destroy-a-socket-connection-in-c
    // We're trying to flush accepted socket contents.  This doesn't seem to help, though
    int v = 1;
    setsockopt(server_fd,SOL_SOCKET,SO_REUSEADDR,&v,sizeof(v));

    server_addr.sin_family = AF_INET;
    server_addr.sin_addr.s_addr = INADDR_ANY;
    server_addr.sin_port = htons(9100);

    if(bind(server_fd, (const sockaddr*) &server_addr, sizeof(server_addr)) < 0)
    {
        perror("bind failed");
        return EXIT_FAILURE;
    }

    if(listen(server_fd, 3) < 0)
    {
        perror("listen failed");
        return EXIT_FAILURE;
    }

    std::cout << "Listening" << std::endl;

    for(;;)
    {
        static int counter = 0;
        int client_fd;
        struct sockaddr_in client_addr;
        socklen_t len = sizeof(client_addr);

        client_fd = accept(server_fd, (sockaddr*) &client_addr, &len);

        std::cout << "Indicating: " << ++counter << std::endl;

        write(client_fd, "HTTP/1.1 200 OK\r\n");
        write(client_fd, "Content-Type: text/html\r\n");
        write(client_fd, "Connection: close\r\n\r\n");

        write(client_fd, "Writing a bunch of stuff!  Here's our number: ");
        write(client_fd, std::to_string(counter).c_str());
        write(client_fd, "\r\n");

        shutdown(client_fd, SHUT_RDWR);
        close(client_fd);
    }

    return 0;
}
