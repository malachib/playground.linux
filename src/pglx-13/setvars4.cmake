set(CMAKE_SYSTEM_NAME Linux)
set(CMAKE_SYSTEM_PROCESSOR arm)
set(CMAKE_SYSROOT ${CMAKE_CURRENT_LIST_DIR}/sysroot)

set(triple arm-linux-gnueabihf)

set(CMAKE_C_COMPILER clang)
set(CMAKE_C_COMPILER_TARGET ${triple})
set(CMAKE_CXX_COMPILER clang++)
set(CMAKE_CXX_COMPILER_TARGET ${triple})

# None of any of these help.  There's no FindQt6 anywhere
#set(CMAKE_MODULE_PATH ${CMAKE_SYSROOT}/usr/share/cmake-3.16/Modules)
# Definitely some goodies here
set(QT_DIR ${CMAKE_SYSROOT}/usr/lib/aarch64-linux-gnu/cmake/Qt6)
set(Qt6_DIR ${QT_DIR} CACHED STRING "")
# Only a handful of goodies here
set(CMAKE_PREFIX_PATH ${CMAKE_SYSROOT}/usr/lib/qt6)
