set(CMAKE_SYSTEM_NAME Linux)
set(LLVM_TARGETS_TO_BUILD arm)
set(LLVM_HOST_TRIPLE arm-linux-gnueabihf)
# Doesn't seem to help, still using GCC LD on C131-debian
# Looks like this is full of nuance, see
# https://discourse.llvm.org/t/trying-to-build-clang-and-runtimes-but-linking-fails-need-help/72851/13
set(LLVM_USE_LINKER lld)

set(CMAKE_SYSROOT ${CMAKE_CURRENT_LIST_DIR}/sysroot)
#set(CMAKE_SYSROOT ${CMAKE_CURRENT_LIST_DIR}/sysroot/lib/arm-linux-gnueabihf)

set(CMAKE_C_COMPILER clang)
set(CMAKE_CXX_COMPILER clang++)

# Needs CMake 3.29+
#set(CMAKE_LINKER_TYPE ldd)
# So instead we use this technique:
# https://stackoverflow.com/questions/1867745/cmake-use-a-custom-linker
# However, we get warnings
# https://stackoverflow.com/questions/73780321/argument-unused-during-compilation-fuse-ld-lld
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -fuse-ld=lld")
# Oddly, this guy fails
#set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -fuse-ld=ld.lld")

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} --target=${LLVM_HOST_TRIPLE}")

set(CMAKE_MODULE_PATH ${CMAKE_SYSROOT}/usr/share/cmake-3.16/Modules)
