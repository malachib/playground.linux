int buffered_span_getc(BufferedSpan* s)
{
    int c = *s->gpos;

}




void accumulator_placement_new(Accumulator* a, Span span)
{
    a->span = span;
    a->accumulated = 0;
}


void accumulator_append_char(Accumulator* a, char c)
{
    *(a->span.buf + a->accumulated) = c;
    ++a->accumulated;
}

void accumulator_append(Accumulator* a, void* buf, unsigned sz)
{
    memcpy(a->span.buf + a->accumulated, buf, sz);
    a->accumulated += sz;
}


void accumulator_reset(Accumulator* a)
{
    a->accumulated = 0;
}


void token_state_machine_placement_new(TokenStateMachine* sm, const char* delim)
{
    sm->delim = delim;
    sm->state = TOKEN_STATE_IDLE;

    // DEBT: For now, we operate in CRLF only mode so LF after CR *always* means EOL
    sm->eol_mode = TOKEN_STATE_EOLMODE_CRLF;
}


void token_state_machine_reset(TokenStateMachine* sm)
{
    sm->state = TOKEN_STATE_IDLE;
}

///
/// @param sm
/// @param c
/// @returns true = regular token character, false = whitespace character, toss
static bool token_state_machine_process(TokenStateMachine* sm, char c)
{
    switch(sm->state)
    {
        case TOKEN_STATE_IDLE:
            switch(c)
            {
                case '\r':
                    sm->state = sm->eol_mode ==
                                TOKEN_STATE_EOLMODE_CR ? TOKEN_STATE_EOL : TOKEN_STATE_CR;
                    break;

                case '\n':
                    if(sm->eol_mode == TOKEN_STATE_EOLMODE_LF)
                        sm->state = TOKEN_STATE_EOL;
                    else
                        sm->state = TOKEN_STATE_LF;
                    break;

                case ' ':
                case '\t':
                    return false;

                default:
                {
                    char* delim = strchr(sm->delim, c);

                    if (delim)
                    {
                        sm->state = TOKEN_STATE_DELIM;
                    }
                    break;
                }
            }
            break;

        case TOKEN_STATE_LF:
            sm->state = TOKEN_STATE_EOL;
            break;

        case TOKEN_STATE_CR:
            if(c == '\n')
            {
                if(sm->eol_mode == TOKEN_STATE_EOLMODE_CRLF)
                    sm->state = TOKEN_STATE_EOL;

                return false;
            }
            break;

            /*
        case TOKEN_STATE_LF:
            // DEBT: For now, we operate in CRLF only mode so LF after CR *always* means EOL
            sm->state = TOKEN_STATE_EOL;
            break; */
    }
}

char* token_state_machine_process_chunk(TokenStateMachine* tsm, char* dest, char *src, int available, unsigned short *pos)
{
    TokenStates _state;

    while((token_state_machine_process(tsm, *src), (_state = tsm->state) == TOKEN_STATE_IDLE) && *pos < available)
    {
        // DEBT: state machine doesn't pay attention to whitespace, but should
        if(*src != ' ')
        {
            dest[*pos] = *src;
            ++*pos;
        }

        ++src;
    }

    // If delim, we want consumer to have a crack at the delimiter so return it
    if(_state == TOKEN_STATE_DELIM) return src;

    // Otherwise, it's EOL and process it here - a feature of the state machine is EOL detection
    // and we eat that up
    ++src;

    // DEBT: EOL discovery should be inside state machine itself, not out here
    while(_state != TOKEN_STATE_EOL)
    {
        token_state_machine_process(tsm, *src);
        _state = tsm->state;
        ++src;
    }

    return src;
}


// Prior state machine which attempted to either chunk or line process in same code


int http_request_state_machine_process(HttpRequestStateMachine* sm, char* buf, int available)
{
    HttpRequestStates state = sm->state;
    StateMachineMessage* m = &sm->message;
    HttpWritableContext* c = &sm->context;
    char* saveptr;
    int retval = 0;

    //LOG_TRACE("http_request sm: %s", buf);

    switch(state)
    {
        case HTTP_REQUEST_INIT:
            state = HTTP_REQUEST_LINE;
            break;

        case HTTP_REQUEST_LINE:
        {
            const char* verb = strtok_r(buf, " ", &saveptr);
            const char* http_str = strtok_r(NULL, " ", &saveptr);
            const char* version = strtok_r(NULL, " \r", &saveptr);

            LOG_DEBUG("http_request verb: %s:%s:%s", verb, http_str, version);

            m->version = version;
            m->method = verb;
            m->path = http_str;

            state = HTTP_REQUEST_HEADERS_START;
            retval = saveptr - buf;

            // DEBT: SUPER sloppy, eats up remainder LF from request line CRLF or
            // from previous header line CR or LF
            if(sm->chunk_mode)
            {
                ++retval;
            }

            break;
        }

        case HTTP_REQUEST_HEADERS_START:
            state = sm->chunk_mode ? HTTP_REQUEST_HEADERS_KEY_CHUNK : HTTP_REQUEST_HEADERS_LINESTART;
            break;

        case HTTP_REQUEST_HEADERS_LINESTART:
            if(*buf == 0)   // we trim whitespace off the end, so empty string really is empty
            {
                /*
                // Empty line means headers are done

                // Now that we've gathered all the headers, get an early peek at what dispatcher
                // wants to handle this request.  Useful for knowing what to do with 'body', plus
                // not too much later we'll need this anyway to perform the dispatch itself
                if(sm->server != NULL)
                    // It's possible to operate state machine without a full server/dispatcher environment
                    sm->dispatcher = http_server_get_dispatcher(sm->server, &sm->const_context);
                */

                state = c->content_length > 0 ? HTTP_REQUEST_BODY : HTTP_REQUEST_DONE;
            }
            else
                // headers continue
                state = HTTP_REQUEST_HEADERS_LINE;

            break;

        case HTTP_REQUEST_HEADERS_KEY_CHUNK:
        {
            // DEBT: if 'read' is bigger than say 64, directly strtok_r on incoming buf

            unsigned short* pos = &sm->secondary_pos;
            TokenStateMachine* tsm = &sm->tsm;

            char* buf_new = token_state_machine_process_chunk(tsm, sm->secondary, buf, available, pos);

            retval = buf_new - buf;

            TokenStates _state = tsm->state;

            if(_state == TOKEN_STATE_DELIM)
            {
                sm->secondary[*pos] = 0;
                m->key = get_http_header_key(sm->secondary, true);
                state = HTTP_REQUEST_HEADERS_KEY_FOUND;

                token_state_machine_reset(&sm->tsm);
                sm->secondary_pos = 0;

                // we don't auto increment past delimiter, so account for that here
                ++retval;
            }
            else if(_state == TOKEN_STATE_EOL)
            {
                // EOL here means extra CRLF so we're on to BODY, if any
                state = HTTP_REQUEST_BODY;
            }

            break;
        }

        case HTTP_REQUEST_HEADERS_KEY_FOUND:
            state = HTTP_REQUEST_HEADERS_VALUE_CHUNK;
            break;

        case HTTP_REQUEST_HEADERS_VALUE_CHUNK:
        {
            unsigned short* pos = &sm->secondary_pos;
            TokenStateMachine* tsm = &sm->tsm;

            char* buf_new = token_state_machine_process_chunk(tsm, sm->secondary, buf, available, pos);

            retval = buf_new - buf;

            if(tsm->state == TOKEN_STATE_EOL)
            {
                sm->secondary[*pos] = 0;
                m->value = sm->secondary;

                if(m->key == HTTP_HEADER_FIELD_CONTENT_LENGTH)
                {
                    sm->context.content_length = atoi(m->value);
                }

                token_state_machine_reset(&sm->tsm);
                sm->secondary_pos = 0;

                // we do auto increment past LF in EOL, so don't increment again
            }
            else
            {
                // TODO: Error State
            }

            state = HTTP_REQUEST_HEADERS_VALUE_FOUND;
            break;
        }

        case HTTP_REQUEST_HEADERS_VALUE_FOUND:
            state = HTTP_REQUEST_HEADERS_LINE;
            break;

        case HTTP_REQUEST_HEADERS_LINE:
        {
            if(sm->chunk_mode)
            {
                state = HTTP_REQUEST_HEADERS_KEY_CHUNK;
                break;
            }

            const char* key = strtok_r(buf, ":", &saveptr);
            const char* value = strtok_r(NULL, " ", &saveptr);

            m->key = key;
            m->value = value;
            m->matched_key = get_http_header_key(key, true);

            if(m->matched_key == HTTP_HEADER_FIELD_CONTENT_LENGTH)
            {
                c->content_length = atoi(value);
                LOG_TRACE("http_request content-length=%u", c->content_length);
            }

            state = HTTP_REQUEST_HEADERS_LINESTART;
            break;
        }

        case HTTP_REQUEST_BODY:
            return http_request_state_machine_process_body(sm, buf, available);

        case HTTP_REQUEST_DONE:
            retval = -1;
            break;
    }

    sm->state = state;
    return retval;
}