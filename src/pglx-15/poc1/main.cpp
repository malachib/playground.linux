#include <iostream>

#include <spdlog/spdlog.h>

#include "static1/lib1.h"
#include "shared1/lib2.h"

using namespace std;

int main()
{
    SPDLOG_INFO("main: entry");

    cout << "Hello World!" << endl;

    {
        Lib1 lib1;
        Lib2 lib2;
    }

    return 0;
}
