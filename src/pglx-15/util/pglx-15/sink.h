#pragma once

#include <memory>

#include <spdlog/sinks/dist_sink.h>
#include <spdlog/sinks/stdout_color_sinks.h>

using dist_sink = std::shared_ptr<spdlog::sinks::dist_sink_mt>;
using spdlog_sink = std::shared_ptr<spdlog::sinks::sink>;


inline dist_sink make_sink()
{
    dist_sink sink = std::make_shared<spdlog::sinks::dist_sink_mt>();
    auto stdout_sink = std::make_shared<spdlog::sinks::stdout_color_sink_mt>();

    sink->add_sink(stdout_sink);

    return sink;
}

