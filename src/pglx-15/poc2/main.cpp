#include <iostream>

#include <spdlog/spdlog.h>

#include "static1/lib1.h"
#include "static2/lib2.h"

using namespace std;

int main()
{
    SPDLOG_INFO("main: entry");

    cout << "Hello World!" << endl;

    {
        Lib1 lib1;
        Lib2 lib2;
    }

    //SPDLOG_TRACE("main: exit");
    SPDLOG_LOGGER_TRACE(spdlog::default_logger(), "main: exit");

    return 0;
}
