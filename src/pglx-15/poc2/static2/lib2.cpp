#include <spdlog/spdlog.h>

#include "lib1.h"
#include "lib2.h"

#include <pglx-15/sink.h>

// Static

Lib2::Lib2()
{
    SPDLOG_INFO("Lib2: ctor");

    auto logger = std::make_shared<spdlog::logger>("xyz", make_sink());

    logger->set_level(spdlog::level::trace);
    spdlog::set_default_logger(logger);

    SPDLOG_TRACE("Lib2: ctor exit");
}
