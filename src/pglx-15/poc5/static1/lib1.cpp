#include <memory>

#include <spdlog/spdlog.h>
#include <spdlog/sinks/dist_sink.h>
#include <spdlog/sinks/stdout_color_sinks.h>

#include "pglx-15/sink.h"

#include "lib1.h"

// Static

Lib1::Lib1()
{
    // TODO: Name this "xyz", then make another "xyz" with a brand new stdout_color_sink_mt
    // Perhaps another Lib1 instance alone is sufficient
    // Clone doesn't count because another stdout_color_sink_mt isn't made
    auto logger = std::make_shared<spdlog::logger>("xyz", make_sink());

    spdlog::set_default_logger(logger);

    SPDLOG_LOGGER_TRACE(logger, "poc2.Lib1: trace1");

    logger = std::make_shared<spdlog::logger>("xyz", make_sink());

    spdlog::set_default_logger(logger);
    logger->set_level(spdlog::level::trace);

    SPDLOG_INFO("poc2.Lib1: ctor");
    SPDLOG_LOGGER_TRACE(logger, "poc2.Lib1: trace2");
}
