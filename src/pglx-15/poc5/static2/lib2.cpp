#include <spdlog/spdlog.h>

#include "lib1.h"
#include "lib2.h"

#include <pglx-15/sink.h>

// Static

Lib2::Lib2()
{
    SPDLOG_INFO("Lib2: ctor");

    auto logger = std::make_shared<spdlog::logger>("xyz", make_sink());

    logger->set_level(spdlog::level::trace);
    spdlog::set_default_logger(logger);

    SPDLOG_TRACE("Lib2: ctor exit");

    // Throws exception "logger with name 'console' already exists" -
    // specifically if both are named 'console' just as github issue
    // indicates [1.2]
    auto console1 = spdlog::stdout_color_mt("console");
    auto console2 = spdlog::stdout_color_mt("console");
}
