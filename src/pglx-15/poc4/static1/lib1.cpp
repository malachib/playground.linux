#include <spdlog/spdlog.h>

#include "lib1.h"
#include "pglx-15/sink.h"

Lib1::Lib1()
{
    SPDLOG_INFO("Lib1: ctor");

    auto logger = std::make_shared<spdlog::logger>("xyz", make_sink());

    spdlog::set_default_logger(logger);
}
