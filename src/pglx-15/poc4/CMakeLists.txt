cmake_minimum_required(VERSION 3.16)

project(pglx-15-poc4 LANGUAGES CXX)

include(../../../tools/cmake/setenv.cmake)

set(CMAKE_POSITION_INDEPENDENT_CODE TRUE)

CPMAddPackage("gh:gabime/spdlog@1.8.1")

add_subdirectory(shared1)
add_subdirectory(static1)

add_executable(${PROJECT_NAME} main.cpp)

# 04DEC24 MB - Not 100% sure this guy is needed, just to make sure .so gets compiled
add_dependencies(${PROJECT_NAME} poc4-shared1)

target_compile_features(${PROJECT_NAME} PUBLIC cxx_std_14)

target_link_libraries(${PROJECT_NAME} poc4-static1 spdlog::spdlog)

include(GNUInstallDirs)
install(TARGETS ${PROJECT_NAME}
    LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR}
    RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR}
)
