#include <iostream>

#include <dlfcn.h>

#include <spdlog/spdlog.h>

#include "static1/lib1.h"
#include "shared1/lib2.h"

using namespace std;

int main()
{
    SPDLOG_INFO("main: entry");

    cout << "POC4: Hello World!" << endl;

    // DEBT: Do something less platform-specific for so location
    void* handle = dlopen("shared1/libpoc4-shared1.so", RTLD_NOW);

    FMT_ASSERT(handle != nullptr, "dlopen load of shared1 failed");

    auto lib2_init = (void(*)()) dlsym(handle, "lib2_init");

    FMT_ASSERT(lib2_init != nullptr, "");

    {
        Lib1 lib1;
        lib2_init();
    }

    dlclose(handle);

    return 0;
}
