#include <spdlog/spdlog.h>

#include "lib2.h"
#include "pglx-15/sink.h"

// Shared / Dynamic

Lib2::Lib2()
{
    SPDLOG_INFO("Lib2: ctor");

    auto logger = std::make_shared<spdlog::logger>("xyz", make_sink());

    spdlog::set_default_logger(logger);
}


extern "C" void lib2_init()
{
    Lib2 lib;
}
