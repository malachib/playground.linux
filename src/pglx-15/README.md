# PGLX-15: spdlog across libraries

Document v0.1

## 1. Scope

## 2. Infrastructre

### 2.1. POC1

One static and one shared lib, spdlog statically linked to both + app

### 2.2. POC2

Two static libs

### 2.3. POC3

One static and one shared lib, spdlog dynamically linked

### 2.4. POC4

[dlsym](https://linux.die.net/man/3/dlsym) shared object dynamic loader test

### 2.5. POC5

Example-driven top-level logger assignment.  Uses static libs

Rather than sink and shared_ptr focused approach, use "direct" logger approaches as seen in [1.2] [1.3]

## 3. Opinions & Observations

### 3.1. POC1

#### 3.1.1. POC1: CMake Settings

If top level app doesn't set `CMAKE_POSITION_INDEPENDENT_CODE` to TRUE, we get:

```text
/usr/bin/ld: _deps/spdlog-build/libspdlog.a(spdlog.cpp.o): relocation R_X86_64_TPOFF32 against `_ZGVZN6spdlog7details2os9thread_idEvE3tid' can not be used when making a shared object; recompile with -fPIC
/usr/bin/ld: failed to set dynamic section sizes: bad value
```

Just like others [1.1]

SPDLOG_BUILD_PIC appears to turn that on also

### 3.2. POC2 RESERVED

### 3.3. POC3 RESERVED

### 3.4. POC4 RESERVED

### 3.5. POC5

#### 3.5.1. POC5: Global behaviors

It seems that instantiating `spdlog::stdout_color_mt` has a global side effect, to set some global name map associating to this logger.  This is where collisions eminate from [1.2]

#### 3.5.2. POC5: Async determination

I never realized before, but it seems that mt sinks are not enough to go full async, one also
needs `make_shared<spdlog::async_logger>` [1.2]

## References

1. https://github.com/gabime/spdlog
    1. https://github.com/gabime/spdlog/issues/2226 
    2. https://github.com/gabime/spdlog/issues/284
    3. https://github.com/gabime/spdlog/blob/v1.x/example/example.cpp
