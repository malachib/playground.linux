#pragma once

#include <vector>

struct vertex;

struct adjVertex{
    vertex *v;
};

struct vertex{
    int vertexNum;
    bool visited = false;
    std::vector<adjVertex> adj;
};


struct graph
{
private:
    int n = 4;
    int** puzzle;
    vertex** vertices;
    std::vector<int> path;

public:
    void createDefaultPuzzle();
    void createPath(int i, int j);
    void printPuzzle();
    std::vector<int> findOpenAdjacentPaths(int x, int y);
    int findVertexNumFromPosition(int x, int y);
    void convertPuzzleToAdjacencyListGraph();
    void addVertex(int num);
    void addEdge(int v1, int v2);
    void displayEdges();

    bool checkIfValidPath();
    bool findPathThroughPuzzle();
};

void graph_test();