// All references in README.md
#include <iostream>
#include <cassert>

#include <unistd.h>     // for fork
#include <sys/mman.h>   // for mmap
#include <sys/wait.h>
#include "stdlib.h"     // for exit
#include "stdio.h"      // so I can read file like she does

#include "director.h"

// Guidance from [6]
// Effectively, 'wait' waits for ONE child process.
// So we keep looping that until no child processes are running (see 'exit')
static void wait_all()
{
    int status = 0;
    int wpid;

    while ((wpid = wait(&status)) > 0);
}

static int grades[3][10] =
{
    {
        19, 9, 12, 3, 0, 15, 20, 17, 13, 10
    },
    {
        17, 6, 11, 7, 5, 13, 18, 19, 15, 13
    },
    {
        20, 10, 10, 9, 8, 15, 18, 19, 14, 18
    }
};

void Worker::process_student_marks(int* marks, int count)
{
    std::string id = "(x";

    id += std::to_string(chapter);
    id += 'y';
    id += std::to_string(y);
    id += ')';

    std::cout << id << "processing student marks: ";
    float sum = 0;
    int i = count;

    while(i--)
    {
        int mark = *marks++;
        std::cout << ' ' << mark;

        sum += mark;
    }

    float avg = sum / count;

    std::cout << std::endl;
    std::cout << id << "average = " << avg << std::endl;
}

void Manager::process_chapter(int homeworks)
{
    std::cout << "Manager::process_chapter: " << chapter << std::endl;

    for(int y = 0; y < homeworks; ++y)
    {
        pid_t pid = fork();

        if(pid == 0)
        {
            Worker worker{director, y, chapter};

            worker.process_student_marks(grades[chapter], 10);

            // child process
            exit(0);
        }
        else
        {

        }
    }

    wait_all();
}

void Director::process_grades()
{
    int chapters = 3;

    int temp[20];

    get_grades(1, temp);

    for(int x = 0; x < chapters; ++x)
    {
        pid_t pid = fork();

        // [5] saved me
        if(pid == 0)
        {
            // child process
            Manager m{this, x};

            m.process_chapter(2);
            exit(0);
        }
    }

    wait_all();
}

int Director::get_grade(int x, int y)
{
    return * (raw_data + (y * columns()) + x);
}


void Director::get_grades(int column, int* g)
{
    for(int i = 0; i < student_count; i++)
    {
        g[i] = get_grade(column, i);
    }
}


int* create_array(int columns, int rows)
{
    return (int*)malloc(rows * columns * sizeof(int));
}

Director::Director()
{
    raw_data = nullptr;
}

Director::~Director()
{
    if(raw_data) free(raw_data);
}

void Director::load(const char* filename, int student_count, int chapter_count, int assignment_count)
{
    FILE* f = fopen(filename, "r");

    if(f == nullptr)
    {
        return;
    }

    int columns = student_count * chapter_count;

    raw_data = create_array(columns, student_count);

    int* temp = raw_data;

    for(int n = 0; n < student_count; n++)
    {
        for (int i = 0; i < columns; i++)
        {
            int val;

            fscanf(f, "%d", &val);

            *temp++ = val;
        }
    }

    int total = temp - raw_data;

    assert(total == columns * student_count);

    this->student_count = student_count;
    this->assignemnt_count = assignment_count;
    this->chapter_count = chapter_count;

    fclose(f);
}

void do_director()
{
    Director d;

    d.load("data/input1", 10, 2, 2);
    d.process_grades();
}