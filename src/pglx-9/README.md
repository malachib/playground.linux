# Overview

# References

1. https://pubs.opengroup.org/onlinepubs/009696799/functions/fork.html
2. https://www.geeksforgeeks.org/fork-memory-shared-bw-processes-created-using/
3. https://www.programiz.com/dsa/graph-dfs
4. CSCI 2270 - Data Structures
5. https://stackoverflow.com/questions/15328285/how-to-fork-an-exact-number-of-children
6. https://stackoverflow.com/questions/19461744/how-to-make-parent-wait-for-all-child-processes-to-finish