#pragma once

struct Director;

// Represents an assignment x#y# (chapter = x, assignment = y)
struct Worker
{
    Director* director;

    int y;
    int chapter;

    void process_student_marks(int* marks, int count);
};

struct Manager
{
    Director* director;

    int chapter;

    void process_chapter(int homeworks);
};

struct Director
{
    int* raw_data;
    int student_count;
    int chapter_count;
    int assignemnt_count;

    int columns() const { return chapter_count * assignemnt_count; }

    int get_grade(int x, int y);
    void process_grades();
    void load(const char* filename, int student_count, int chapter_count, int assignment_count);
    void get_grades(int column, int* g);

    Director();
    ~Director();
};

void do_director();