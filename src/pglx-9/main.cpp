#include <iostream>

#include "director.h"
#include "graph.h"

#include <unistd.h>     // for fork
#include <sys/mman.h>   // for mmap
#include <sys/wait.h>

struct Shared
{
    int value;
};

static void basic_test()
{
    pid_t p;

    int not_shared = 0;

    // Guidance from [2]
    auto shared = (Shared*) mmap(NULL, sizeof(Shared),
                                 PROT_READ | PROT_WRITE,
                                 MAP_SHARED | MAP_ANONYMOUS,
                                 -1, 0);

    shared->value = 0;

    if(p = fork() == 0)
    {
        // in child process
        std::cout << "Hello from child!" << std::endl;

        not_shared = 10;
        shared->value = 11;
    }
    else
    {
        // Guidance from [2]
        // "wait for child to die"
        wait(nullptr);

        // in parent process
        std::cout << "shared: " << shared->value << std::endl;
        std::cout << "not shared: " << not_shared << std::endl;
    }

    munmap(shared, sizeof(Shared));
}

int main()
{
    //basic_test();

    do_director();
    //graph_test();

    return 0;
}
