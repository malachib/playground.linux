#include <iostream>
#include <stack>

#include "graph.h"

void graph::createDefaultPuzzle()
{
    puzzle = new int* [n];
    vertices = new vertex*[n * n];

    for(int i = 0; i < n; i++)
    {
        int* row = puzzle[i] = new int[n];
        for(int j = 0; j < n; j++)
            row[j] = 1;
    }

    puzzle[0][0] = 0;
    puzzle[n-1][n-1] = 0;
}

void graph::printPuzzle()
{
    for(int i = 0; i < n; ++i)
    {
        for(int j = 0; j < n; ++j)
        {
            std::cout << "| " << puzzle[i][j] << ' ';
        }

        std::cout << '|' << std::endl;
    }
}

void graph::createPath(int i, int j)
{
    puzzle[i][j] = 0;
}


inline int getVertexNum(int x, int y, int n)
{
    //return y + n * x;
    return x + y * n;
}

int graph::findVertexNumFromPosition(int x, int y)
{
    return getVertexNum(x, y, n);
}

void graph::addVertex(int num)
{
    vertex** current = &vertices[num];
    if(*current == nullptr)
    {
        *current = new vertex();
        (**current).vertexNum = num;
    }
}

void graph::addEdge(int v1, int v2)
{
    vertex* _v1 = vertices[v1];
    vertex* _v2 = vertices[v2];

    // DEBT: This pseudo lazy init feels wrong, assignment [4] mentions nothing about this
    // If we DO keep it, merely call addVertex at the top since it also lazy inits
    if(_v1 == nullptr)
    {
        addVertex(v1);
        _v1 = vertices[v1];
    }

    if(_v2 == nullptr)
    {
        addVertex(v2);
        _v2 = vertices[v2];
    }

    _v1->adj.push_back(adjVertex{_v2});
    _v2->adj.push_back(adjVertex{_v1});
}

static void puzzle_helper(int** puzzle, int x, int y, int n, std::vector<int>& adjacent)
{
    if(x < 0 || y < 0) return;
    if(x >= n || y >= n) return;

    if(puzzle[x][y] == 0)
        adjacent.push_back(getVertexNum(x, y, n));
}

std::vector<int> graph::findOpenAdjacentPaths(int x, int y)
{
    std::vector<int> adjacent;

    puzzle_helper(puzzle, x - 1, y - 1, n, adjacent);
    puzzle_helper(puzzle, x, y - 1, n, adjacent);
    puzzle_helper(puzzle, x + 1, y - 1, n, adjacent);

    puzzle_helper(puzzle, x - 1, y, n, adjacent);
    //puzzle_helper(puzzle, x, y, n, adjacent); // don't include ourself
    puzzle_helper(puzzle, x + 1, y, n, adjacent);

    puzzle_helper(puzzle, x - 1, y + 1, n, adjacent);
    puzzle_helper(puzzle, x, y + 1, n, adjacent);
    puzzle_helper(puzzle, x + 1, y + 1, n, adjacent);

    return adjacent;
}

void graph::convertPuzzleToAdjacencyListGraph()
{
    for(int x = 0; x < n; ++x)
    {
        for(int y = 0; y < n; ++y)
        {
            if(puzzle[x][y] == 0)
            {
                int vertexNum = findVertexNumFromPosition(x, y);

                std::vector<int> adjacent = findOpenAdjacentPaths(x, y);

                addVertex(vertexNum);

                for(int edge : adjacent)
                {
                    addEdge(vertexNum, edge);
                }
            }
        }
    }
}

struct helper
{
    std::vector<int>& visited;
    //std::stack<vertex*> stack;
    vertex** const vertices;
    const int last_vertex;

    /*
    void dfs(vertex* v)
    {
        v->visited = true;
        visited.push_back(v);   // NOTE: Don't think we need to do this

        for(auto adjacent : v->adj)
            stack.push(adjacent.v);

        while(!stack.empty())
        {
            dfs(stack.top());
            stack.pop();
        }
    } */
    // Guidance from [3]
    // presumes 'v' is not yet visited
    // since graphs technically could be windy, I am returning a 'reached end' bool.  Not 100% sure that's needed though
    // given the way we build the graph
    bool dfs(vertex* v)
    {
        std::cout << "Reached vertex: " << v->vertexNum << std::endl;

        v->visited = true;

        visited.push_back(v->vertexNum);

        if(v->vertexNum == last_vertex) return true;

        for(auto adjacent : v->adj)
        {
            if(!adjacent.v->visited)
            {
                bool done = dfs(adjacent.v);
                if(done) return true;
                // Not 100% sure this is the right spot for this, but I think it is
                std::cout << "Backtracked to vertex: " << v->vertexNum << std::endl;
            }
        }

        return false;
    }
};

bool graph::checkIfValidPath()
{
    int last_vertex = n * n - 1;
    return vertices[path.back()]->vertexNum == last_vertex;
}

bool graph::findPathThroughPuzzle()
{
    path.clear();

    helper h{path, vertices, n * n - 1};

    std::cout << "Starting at vertex: 0" << std::endl;

    h.dfs(vertices[0]);

    return checkIfValidPath();
}

static void test1(graph& g)
{
    g.addVertex(0);
    g.addVertex(5);
    g.addVertex(10);
    g.addVertex(15);

    g.addEdge(0, 5);
    g.addEdge(5, 10);
    g.addEdge(10, 15);
}

void graph_test()
{
    graph g;

    g.createDefaultPuzzle();
    g.createPath(1, 1);
    g.createPath(2, 2);

    g.convertPuzzleToAdjacencyListGraph();
    bool isValid = g.findPathThroughPuzzle();

    std::cout << "isValid: " << isValid << std::endl;

    g.printPuzzle();
}