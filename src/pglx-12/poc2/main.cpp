#include <iostream>

#include <estd/expected.h>
#include <estd/system_error.h>

using namespace std;
using expected = estd::expected<int, std::errc>;

void handler(expected e)
{
    if(e.has_value())
        cout << "Has value: " << e.value();
    else
        cout << "Has error: " << (int) e.error();

    cout << endl;
}

int main()
{
    expected e = 0;

    handler(e);
    handler(estd::unexpected{errc::bad_address});
    //handler(expected::unexpected_type{errc::address_in_use});
    handler(expected(estd::unexpect_t{}, errc::bad_address));

    return 0;
}
