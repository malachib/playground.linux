# DO NOT USE

This project is now superseded by https://github.com/malachi-iot/http-server

## Notes

# References

1. https://www.thegeekstuff.com/2012/05/c-mutex-examples/
2. https://gist.github.com/alexklibisz/7cffdfe90c97986f8393
3. https://docs.oracle.com/cd/E19455-01/806-5257/6je9h032r/index.html#sync-44265
4. https://www.ibm.com/docs/en/aix/7.2?topic=p-pthread-cond-signal-pthread-cond-broadcast-subroutine
5. https://man7.org/linux/man-pages/man3/pthread_create.3.html
6. https://stackoverflow.com/questions/63985373/cmake-error-undefined-reference-to-pthread-create
7. https://stackoverflow.com/questions/21091000/how-to-get-thread-id-of-a-pthread-in-linux-c-program
8. https://stackoverflow.com/a/20382011/287545
9. https://docs.oracle.com/cd/E37838_01/html/E61057/sync-21067.html#OSMPGsync-30944