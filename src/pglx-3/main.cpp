#include <iostream>

// https://github.com/zeromq/cppzmq
#include <zmq.hpp>  // NOTE: On Debian, this is actually already present.  However, we pull in via conan anyway
#include <zmqpp/zmqpp.hpp>

// Starting with https://github.com/zeromq/cppzmq/blob/master/examples/hello_world.cpp
int main()
{
    zmq::context_t ctx;
    zmq::socket_t sock(ctx, zmq::socket_type::push);
    sock.bind("inproc://test");
    zmq::socket_t client(ctx, zmq::socket_type::pull);
    client.connect("inproc://test");

    sock.send(zmq::str_buffer("Hello, world"), zmq::send_flags::dontwait);
    //std::cout << "Hello, World!" << std::endl;
    zmq::message_t m;
    zmq::recv_result_t result = client.recv(m);
    std::cout << "Got: " << m.to_string() << std::endl;
    return 0;
}
