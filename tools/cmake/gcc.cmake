add_compile_options(-Werror=return-type)
# This helps assert() not generate warnings
add_compile_options(-Wno-unused-value)
